# SunP04/wcat

A simple and fast `cat` command.

### Attention

This program is meant to be simple and with low to no dependencies, so it only has basic features.
The following commands are native and don't need to be installed and have more features.
- Mac: [cat](https://ss64.com/mac/cat.html)
- Linux/Unix: [cat](https://www.man7.org/linux/man-pages/man1/cat.1.html)
- Windows(Powershell): [Get-Content](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-content)
- Windows(CMD): [type](https://learn.microsoft.com/en-us/windows-server/administration/windows-commands/type)


## Usage

`wcat [FILES]`

## License

MIT License can be found at [LICENSE](LICENSE)
