mod file_reader;

use std::{env, fs::File, io, process};


/// Prints an empty line. Can be used to separate some content of the code.
fn empty_line() {
    eprintln!();
}

fn main() {
    let file_paths = env::args().skip(1);

    if file_paths.len() == 0 {
        eprintln!("No files given!");
        empty_line();
        process::exit(1);
    }

    for name in file_paths {
        println!("*> {name}");

        let mut file = match File::open(&name) {
            Ok(f) => f,
            Err(e) => {
                eprintln!("Could not read file contents.");
                eprintln!("Does the file \"{name}\" exist?");
                eprintln!("[Error] {e}");
        empty_line();
                continue;
            }
        };

        if let Err(e) = io::copy(&mut file, &mut io::stdout()) {
            eprintln!("Could not copy file contents to stdout.");
            eprintln!("[Error] {e}");
            empty_line();
            continue;
        }

        empty_line();
    }

    process::exit(0);
}
